#!/bin/bash

export BASE=${1:-$HOME/Developer/personal/dotfiles}

printf "\e[1;36m  [i]\e[0;36m 'Descargando los dotfiles\e[0m'\n"

if [ ! -d "$BASE" ]; then
mkdir -p "$BASE"
fi

cd $BASE

git clone https://gitlab.com/jmanuelrosa/dotfiles.git . &> /dev/null

source setup/install.sh

print_info "Cambiando la ruta del repositorio de HTTPS a SSH ..."
git remote set-url origin git@gitlab.com:jmanuelrosa/dotfiles.git