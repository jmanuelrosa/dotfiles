#!/bin/bash

init() {
  # Ref: https://www.ostechnix.com/how-to-mount-google-drive-locally-as-virtual-file-system-in-linux/
  local STORAGE_SERVICE="rclone.service"
  local STORAGE_CONFIG="rclone.conf"
  local STORAGE_DIR="${HOME}/.config/rclone"
  local SYSTEM_SERVICES="/etc/systemd/system"

  local STORAGE="
  rclone
  "

  print_download "Instalando aplicaciones de almacenamiento ..."
  yay -S --noconfirm ${BROWSERS}

  print_info "Configurando Google Drive y otros servicios ..."
  ln -sf ${BASE}/config/${STORAGE_CONFIG} $STORAGE_DIR

  print_info "Configurando el servicio ..."
  ln -sf ${BASE}/services/${STORAGE_SERVICE} $SYSTEM_SERVICES

  print_success "Instalación y configuración de las apps finalizada"
}

init
