#!/bin/bash

init() {
  local I3_DIR=${HOME}/.i3
  local I3STATUS_DIR=${HOME}/.config/i3status
  local PICTURES_DIR=${HOME}/Imagenes
  local BIN=/usr/local/bin/

  local BACKUP_DIR=${BASE}/.backup/i3

  DIRS="
  Documentos
  Música
  Plantillas
  Público
  Vídeos"

  FILES="
  .config/i3status/config
  .i3/config"

  ask "Quiere crear una copia de seguridad de los ficheros de configuración de i3?"
  if [[ $REPLY == [sS]* ]]; then
    print_info "Guardando una copia de seguridad de los ficheros que se van a sobreescribir ..."
    createDir $BACKUP_DIR
    for i in ${FILES[@]}; do
      if [ -f ${HOME}/${i} ]; then
        cp ${HOME}/${i} $BACKUP_DIR
      fi
    done
  fi

  print_info "Configurando el sistema de forma personalizada ..."

  for i in ${DIRS[@]}; do
    rm -rf ${HOME}/${i}
  done

  print_info "Eliminando directorios innecesarios ..."

  print_info "Configurando el fondo de pantalla ..."
  rm -rf ${HOME}/Imágenes
  ln -sf ${BASE}/wallpapers $PICTURES_DIR

  source sh/background

  print_info "Configurando i3 ..."
  createDir $I3_DIR
  ln -sf ${BASE}/config/i3/config $I3_DIR
  createDir $I3STATUS_DIR
  ln -sf ${BASE}/config/i3status/config $I3STATUS_DIR

  print_info "Instalando los scripts sh personalizados ..."
  sudo ln -sf ${BASE}/sh/* $BIN

  print_success "Configuración personalizada del sistema finalizada"
}

init
