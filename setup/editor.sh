#!/bin/bash

init() {
  local EDITOR="
  visual-studio-code-bin
  "

  local PLUGINS="
  ms-vscode.atom-keybindings
  "

  local FONTS="
  ttf-fira-code
  "

  print_download "Instalando editor ..."
  yay -S --noconfirm ${EDITOR}

  print_download "Instalando los plugins de VSCode ..."
  code=""
  for i in ${PLUGINS[@]}; do
    code+=" --install-extension ${i}"
  done
  code ${code}

  print_download "Instalando las fuentes ..."
  yay -S --noconfirm ${FONTS}

  print_success "Instalación y configuración del editor finalizada"
}

init
