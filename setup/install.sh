#!/bin/bash

source setup/.utils

print_info "Dotfiles para Manjaro (arch con i3)"

confirm "¡Atención! Este script instalará algunas dependencias y configuraciones en el sistema, ¿está seguro de que desea continuar?"
printf "\n"

print_info "Actualizando los permisos del directorio ..."
sudo find . -name *.sh -type f -exec chmod -R +x {} \;

print_download "Actualizando los repositorios ..."
sudo pacman-mirrors -f8
sudo pacman -Syyu
sudo pacman -Sy
yay -Sy

print_info "Instalando y configurando el sistema ..."
source setup/system.sh
source setup/configuration.sh

print_info "Instalando y configurando las aplicaciones ..."
source setup/apps.sh

print_info "Instalando y configurando las aplicaciones de desarrollo ..."
source setup/git.sh
source setup/development.sh

print_info "Instalando y configurando el terminal de linux ..."
source setup/terminal.sh

print_download "Actualizando todos los paquetes del sistema ..."
yay -Syu --noconfirm

print_error "Eliminando dependencias innecesarias ..."
yay -Yc

print_download "Eliminando la cache ..."
yay -Sc
