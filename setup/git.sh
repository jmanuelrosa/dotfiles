#!/bin/bash

init() {
  local APPS="
  git
  github-cli
  git-delta-bin
  "
  local GIT_DIR="git"
  local FILES="
  .gitconfig
  .gitattributes
  .gitignore
  "

  print_download "Instalando git ..."
  yay -S --noconfirm ${APPS}

  print_info "Configurando git ..."
  for i in ${FILES[@]}; do
    ln -sf ${BASE}/${GIT_DIR}/${i} $HOME
  done

  gh config set git_protocol ssh

  print_info "Creando el fichero de credenciales ..."
  echo "[user]
name = José Manuel Rosa
email = josemanuel.rosamoncayo+github@gmail.com
" > ~/.gitconfig.local

  print_success "Instalación y configuración de git finalizada"
}

init
