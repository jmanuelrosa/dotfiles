#!/bin/bash

init() {
  local BASICS="
  clipit
  expect
  gparted
  pygmentize
  extra/pavucontrol
  pulseaudio
  zsh
  "

  local BLUETOOTH="
  bluez
  bluez-utils
  blueman
  "

  local SSH_DIR=${HOME}/.ssh/

  print_download "Instalando yay como gestor de paquetes ..."
  sudo pacman -S yay
  yay -Sy

  print_download "Instalando aplicaciones básicas del sistema ..."
  yay -S --noconfirm ${BASICS}

  print_download "Instalando bluetooth ..."
  yay -S --noconfirm ${BLUETOOTH}

  print_info "Cargando las claves ssh ..."
  createDir $SSH_DIR
  ln -sf ${BASE}/.ssh/id_rsa $SSH_DIR
  ln -sf ${BASE}/.ssh/id_rsa.pub $SSH_DIR

  print_info "Cambiando los permisos de lad claves ssh ..."
  chmod 400 ${SSH_DIR}/id_rsa

  print_info "Añadiendo permisos a las claves ssh"
  chmod 400 $SSH_DIR/id_rsa

  # ¿Debería tener claves propias gpg?
  # print_info "Cargando las claves gpg publicas y privadas ..."
  # Exportar
  #   gpg --list-secret-keys
  #   gpg --export-secret-keys ID > josemanuel.rosamoncayo@gmail.com.asc
  # gpg --allow-secret-key-import --import ${BASE}/gpg/josemanuel.rosamoncayo@gmail.com.asc

  print_info "Configurando los watchers para node ..."
  echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.d/99-sysctl.conf && sudo sysctl --system

  # Posibles errores con la pantallas
  # https://wiki.archlinux.org/index.php/intel_graphics#DRI3_issues
  # https://wiki.archlinux.org/index.php/intel_graphics#X_freeze.2Fcrash_with_intel_driver
  # print_info "Forzando DR2 para evitar que algunas apps se queden congeladas ..."
  # echo "Section \"Device\"
  #   Identifier \"Intel Graphics\"
  #   Driver \"intel\"
  #   Option \"DRI\" \"2\"
  #   Option \"NoAccel\" \"True\"
# EndSection" | sudo tee -a /etc/X11/xorg.conf.d/20-intel.conf

  print_info "Activando/desactivando servicios ..."
  sudo systemctl enable fstrim.timer
  sudo systemctl start fstrim.timer

  sudo systemctl stop bluetooth.service avahi-daemon.service ModemManager.service org.cups.cupsd.service
  sudo systemctl disable bluetooth.service avahi-daemon.service ModemManager.service org.cups.cupsd.service

  print_success "Instalación y configuración del sistema finalizada"
}

init
