#!/bin/bash

init() {
  local UTILS_DIR=${BASE}/utils/
  local ALIAS_DIR=${BASE}/.aliases
  local BACKUP_DIR=${BASE}/.backup/system
  local TERMINATOR_CONFIG=${HOME}/.config/terminator/

  local TERMINAL="
  terminator
  "

  print_download "Instalando aplicacion del terminal ..."
  yay -S --noconfirm ${TERMINAL}

  print_info "Configurando terminator ..."
  createDir $TERMINATOR_CONFIG
  ln -sf ${BASE}/config/terminator/config $TERMINATOR_CONFIG

  print_download "Descargando antigen ..."
  # https://github.com/zsh-users/antigen
  # Antigen
  createDir $UTILS_DIR
  curl -s -L git.io/antigen > ${UTILS_DIR}antigen.sh

  FILES="
  .antigenrc
  .bash_profile
  .bash_prompt
  .bashrc
  .exports
  .inputrc
  .profile
  .zshrc"

  ask "Quiere crear una copia de seguridad de los ficheros de configuración?"
  if [[ $REPLY == [sS]* ]]; then
    print_info "Guardando una copia de seguridad de los ficheros que se van a sobreescribir ..."
    createDir $BACKUP_DIR
    for i in ${FILES[@]}; do
      if [ -f ${HOME}/${i} ]; then
        cp ${HOME}/${i} $BACKUP_DIR
      fi
    done
  fi

  print_info "Creando (enlazando) los nuevos ficheros ..."
  for i in ${FILES[@]}; do
    ln -sf ${BASE}/${i} $HOME
  done

  ln -sf $ALIAS_DIR $HOME

  print_info "Estableciendo ZSH como la Shell por defecto"
  chsh -s /bin/zsh

  print_success "Instalación y configuración del terminal finalizada"
}

init
