#!/bin/bash

init() {
  local BROWSERS="
  brave-bin
  firefox-developer-edition
  google-chrome
  "

  local BROWSER_CONFIG="mimeapps.list"
  local BROWSER_DIR="${HOME}/.local/share/applications"

  local OTHERS="
  gimp
  qbittorrent
  rar
  spotify
  "

  local REMOVE_APPS="
  deluge
  hexchat
  palemoon-bin
  xfburn
  "

  print_download "Instalando navegadores ..."
  yay -S --noconfirm ${BROWSERS}

  print_info "Estableciendo Chrome como navegador por defecto ..."
  ln -sf ${BASE}/config/${BROWSER_CONFIG} $BROWSER_DIR

  print_download "Instalando otras apps ..."
  curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | gpg --import -
  yay -S --noconfirm ${OTHERS}
  sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
  sudo chmod a+rx /usr/local/bin/youtube-dl

  print_download "Eliminando las aplicaciones innecesarias ..."
  yay --remove --noconfirm ${REMOVE_APPS}

  print_success "Instalación y configuración de las apps finalizada"
}

init
