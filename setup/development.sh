#!/bin/bash

init() {
  local DEVOPS="
  ansible
  docker
  docker-compose
  "

  local TOOLS="
  dbeaver
  insomnia-bin
  "

  print_download "Instalando ansible, docker ..."
  yay -S --noconfirm ${DEVOPS}

  print_info "Configurando docker ..."
  sudo groupadd docker
  sudo usermod -aG docker $USER

  print_download "Instalando la última version lts de node ..."
  nvm install --lts --alias=default
  ln -sf ${BASE}/npm/.npmrc $HOME

  print_download "Instalando otros tools ..."
  yay -S --noconfirm ${TOOLS}

  print_download "Eliminando la cache ..."
  yay --remove -Sc

  print_success "Instalación y configuración de las apps de desarrollo finalizada"
}

init
