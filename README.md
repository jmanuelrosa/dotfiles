# dotfiles
by José Manuel Rosa Moncayo

## Use

Use this dotfiles to configure your system, based on __arch + i3__, with a custom settings for system and some applications.

If you want install all apps and use all configurations, use:

```bash
> bash <(wget -qO- https://gitlab.com/jmanuelrosa/dotfiles/raw/master/init.sh)
```

This file, will run installation and configuration files.

`./bin/install.sh` will install all apps in our system, like this:

* clipit
* docker
* docker-compose
* gimp
* git
* google-chrome
* gparted
* nvm
* pygmentize
* terminator
* spotify
* visual-studio-code-bin
* zsh

After install all apps, execute `configuration.sh` file, it will load a basic configuration for our system.

### Git Config
.gitconfig.local
```
[user]
name = José Manuel Rosa
email = josemanuel.rosamoncayo@gmail.com
```

#### Ideas
* Eliminar Terminator y usar xterm con tmux?
* Porque sale tantas veces establecer chrome como navegor por defecto?